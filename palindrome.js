const palindrom = (charLength) => {
  if (!charLength) return '';
  if (charLength < 0) return '';
  const palindromeWordArr = generatePalindromeArray(charLength);
  return mapArrayToString(palindromeWordArr);
}

const generatePalindromeArray = (charLength) => {
  const palindromeWordArr = new Array(charLength);
  let i = 0;
  while(charLength / 2 > i) {
    const newChar = generateRandomChar();
    palindromeWordArr[i] = newChar;
    palindromeWordArr[charLength - (i + 1)] = newChar;
    i++;
  }
  return palindromeWordArr;
}

const generateRandomChar = () => {
  const char = 'abcdefghijklmnopqrstuvwxyz0123456789';
  return char[Math.floor(Math.random() * char.length)]; 
}

const mapArrayToString = (arr) => {
  let palindromeWord = '';
  for (let i = 0; i < arr.length; i++) {
    palindromeWord += arr[i];
  }
  return palindromeWord;
}

console.log(palindrom(5));