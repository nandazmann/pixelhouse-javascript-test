const numeric = [10,9,102,66,5421,1,0];
const string = ['Wulan','Raharjo','Widya','Yuda','Cinta','Iskandar','Hidayat','Kusuma','Indah','Jusuf'];
const alphanumeric = ['Wulan','Raharjo','Widya',10,9,102,66,5421,1,0,'Yuda','Cinta','Iskandar','Hidayat','Kusuma','Indah','Jusuf'];

const DESC = 'DESC';
const ASC = 'ASC';
const NUMBER = 'number';
const STRING = 'string';

const customSort = (arr, order) => {
  let sorted = false;
  const isAlphaNumeric = checkAlphaNumeric(arr);
  const compareFunction = isAlphaNumeric ? compareAlphaNumeric : compareDefault;
  while(!sorted) {
    let swapped = false;
    for (let i = 0; i < arr.length - 1; i++) {
      if (compareFunction(arr[i], arr[i + 1], order)) {
        const temp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = temp;
        swapped = true;
      }
    }
    // if there is no swapped value, sorting is finished
    sorted = !swapped;
  }
  return arr;
}

const checkAlphaNumeric = (arr) => {
  let isNumber = false;
  let isString = false;
  for (let i = 0; i < arr.length; i++) {
    if (typeof arr[i] === NUMBER) {
      isNumber = true;
    }
    if (typeof arr[i] === STRING) {
      isString = true;
    }
  }
  return isNumber && isString;
}

const compareDefault = (a, b, order) => {
  if (order === ASC) {
    return a > b;
  }
  return a < b;
};

const compareAlphaNumeric = (a, b, order) => {
  if (typeof a === NUMBER ) {
    if (typeof b === NUMBER) {
      return compareDefault(a, b, order);
    } else if (typeof b === STRING){
      if (order === ASC) {
        return false;
      }
      return true;
    }
  } else if (typeof a === STRING) {
    if (typeof b === STRING) {
      return compareDefault(a, b, order);
    } else if (typeof b === NUMBER){
      if (order === ASC) {
        return true;
      }
      return false;
    }
  }
}

const sortDesc = (arr) => {
  return customSort(arr, DESC);
}

const sortAsc = (arr) => {
  return customSort(arr, ASC);
}

console.log(sortAsc(alphanumeric))
console.log(sortAsc(numeric))
console.log(sortDesc(string))