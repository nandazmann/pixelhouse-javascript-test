const randomArrayData = (arrLength, dataPerIndex) => {
  let result = new Array(arrLength);

  for (let i = 0; i < arrLength; i++) {
    result[i] = dataPerIndex ? getRandomData(dataPerIndex) : null;
  }
  return result;
}

const getRandomData = (dataPerIndex) => {
  if (dataPerIndex > 10) {
    dataPerIndex = 10;
  }
  const stringArr = ",Wulan,Raharjo,Widya,Yuda,Cinta,Iskandar,Hidayat,Kusuma,Indah,Jusuf,";
  const delimiters = ',';
  let delimitersCounter = 0;
  let isBeginningWord = false;
  let randomUpperBound = 10 - dataPerIndex;

  let name = '';
  let random = Math.floor(Math.random() * randomUpperBound) + 1;
  let countData = 0;

  for (let i = 0; i < stringArr.length; i++) {
    // check if is not the beginning of new word
    // if is not the beginning and delimiterCounter as random then begin newWord
    if (!isBeginningWord && delimitersCounter === random) {
      isBeginningWord = !isBeginningWord;
    }
    if (delimiters === stringArr[i]) {
      // how many delimiters (in this case ',') till match with random
      delimitersCounter++;
      // if its beginning word and found delimiters, it means end of the new word
      if (isBeginningWord) {
        // reset
        isBeginningWord = !isBeginningWord;
        // count needed data
        countData++;
        if (countData !== dataPerIndex) {
          name += ' ';
        } else {
          break;
        }

        randomUpperBound -= delimitersCounter;
        // reset random and delimitersCounter
        // add insertedData total for randomUpperbound, since at the beginning we remove it with dataPerIndex
        random = Math.floor(Math.random() * (randomUpperBound + countData)) + 1;
        delimitersCounter = 0;

      }
    }

    if (isBeginningWord) {
      // add letter one by one till next delimiter
      name += stringArr[i];
    }

  }
  return name;
}

console.log(randomArrayData(2, 3))
